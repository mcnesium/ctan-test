FROM alpine:latest

RUN apk add perl; \
    wget https://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz; \
    tar -xzf install-tl-unx.tar.gz; \
    cd install-tl-*; \
    while ! perl ./install-tl ; do sleep 1; done